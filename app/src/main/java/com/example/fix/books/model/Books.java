package com.example.fix.books.model;


import com.example.fix.books.R;

import org.parceler.Parcel;

import io.realm.BooksRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;


@RealmClass
//для передачи данных
@Parcel(implementations =
        {BooksRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {Books.class})
//POJO (Plain Old Java Object)
public class Books extends RealmObject {
    private String photo;
    private String authorpic;
    private String rate;
    private String rating;
    @PrimaryKey
    private String bookid;
    private String name;
    private String secondphoto;
    private String time;
    private String genre;
    private String title;
    private String gallery;
    private String pages;
    private String author;


    private int back;
    private int img;

    public Books() {
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }



    public int getBack() {
        int rating1 = Integer.valueOf(rate);
        if (rating1 >= 90) {
            return R.color.hight;
        } else if (rating1 < 85) {
            return R.color.low;
        }else if (rating1 >= 85 && rating1 < 90){
            return R.color.orange;
        }
        return back;
    }

    public int getImg() {
        int rating1 = Integer.valueOf(rate);
        if (rating1 >= 85) {
            return R.drawable.quality;
        }
        return img;
    }

    public String getAuthorpic() {
        return authorpic;
    }

    public void setAuthorpic(String authorpic) {
        this.authorpic = authorpic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }


    public void setTime(String time) {
        this.time = time;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSecondphoto() {
        return secondphoto;
    }

    public void setSecondphoto(String secondphoto) {
        this.secondphoto = secondphoto;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String  id) {
        this.bookid = id;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
